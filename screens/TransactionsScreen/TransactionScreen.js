import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {SearchBar} from "../../components/SearchBar";

import ProgressCircle from "react-native-progress-circle";

export const TransactionScreen=()=>{
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={{fontWeight: "bold",
                        fontSize:40}}>Transactions</Text>
                    <SearchBar/>
                </View>
                <View style={styles.performance}>
                    <Text style={{fontWeight: "bold", fontSize: 20}}>Performance</Text>
                    <View style={styles.progress}>
                        <View >
                        <Text style={styles.name}>Current Week</Text>
                        <ProgressCircle
                            percent={64}
                            radius={50}
                            borderWidth={7}
                            color={"#87e3b3"}
                            shadowColor="#dfe3e2"
                            bgColor="#fff"
                        >
                            <Text
                                weight={"medium"}
                                style={{ fontSize: 18,color: "#87e3b3" }}>
                                64%
                            </Text>
                        </ProgressCircle></View><View>
                        <Text style={styles.name}>Last Week</Text>
                        <ProgressCircle
                            percent={40}
                            radius={50}
                            borderWidth={7}
                            color={"#f9747d"}
                            shadowColor="#dfe3e2"
                            bgColor="#fff"
                        >
                            <Text
                                weight={"medium"}
                                style={{ fontSize: 18,color: "#f9747d" }}>
                                40%
                            </Text>
                        </ProgressCircle></View><View>
                        <Text style={styles.name}>Last Month</Text>
                        <ProgressCircle
                            percent={90}
                            radius={50}
                            borderWidth={7}
                            color={"#606af9"}
                            shadowColor="#dfe3e2"
                            bgColor="#fff"
                        >
                            <Text
                                weight={"medium"}
                                style={{ fontSize: 18,color: "#606af9" }}>
                                90%
                            </Text>
                        </ProgressCircle></View>
                </View>
                <View style={styles.transactions}>

                </View>
            </View>
            </View>
        )
};
const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingHorizontal:20,
    },
    header:{
        flex:1,
        flexDirection: "column",
        justifyContent: "center",

    },
    performance:{
        flex:1,
        justifyContent:"space-around"
    },
    progress:{
        flexDirection:"row",
        justifyContent:"space-between",

    },
    transactions:{
        flex:2,

    },
    headerText:{
        fontWeight: "bold",
        fontSize:40,

    },
                name: {
                marginBottom: 15,
                color: "#939393"
            }
});