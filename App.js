import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {TransactionScreen} from "./screens/TransactionsScreen/TransactionScreen";

export default function App() {
  return (
    <TransactionScreen/>
  );
}

const styles = StyleSheet.create({

});
